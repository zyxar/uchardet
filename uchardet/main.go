package main

import (
    "bitbucket.org/zyxar/uchardet"
    "flag"
    "fmt"
    "io/ioutil"
    "os"
)

func main() {
    flag.Parse()
    if flag.NArg() < 1 {
        fmt.Fprintf(os.Stderr, "file?\n")
        return
    }
    file := flag.Arg(0)
    buf, err := ioutil.ReadFile(file)
    if err != nil {
        fmt.Fprintf(os.Stderr, "error in reading file: %v\n", file)
        return
    }
    a := make([]interface{}, 1)
    a[0] = buf
    ch, er := uchardet.Detect(a)
    if er != nil {
        fmt.Printf("error: %v\n", er)
        return
    }
    fmt.Printf("charset: %v\n", ch)
}
