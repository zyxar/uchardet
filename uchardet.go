package uchardet

/*
#cgo CFLAGS: -I/usr/local/include
#cgo LDFLAGS: -L/usr/local/lib -luchardet
#include <uchardet/uchardet.h>
#include <stdlib.h>
*/
import "C"
import (
    "fmt"
    "unsafe"
)

func Detect(ds []interface{}) (string, error) {
    dt := NewDetector()
    defer dt.Delete()
    for _, data := range ds {
        switch d := data.(type) {
        case string:
            dt.Handle(d)
        case []byte:
            dt.Handle(string(d))
        default:
            continue
        }
    }
    dt.End()
    return dt.Charset()
}

type Detector struct {
    ud C.uchardet_t
}

func (self *Detector) Delete() {
    C.uchardet_delete(self.ud)
}

func (self *Detector) Handle(data string) error {
    d := C.CString(data)
    defer C.free(unsafe.Pointer(d))
    r := C.uchardet_handle_data(self.ud, d, C.size_t(len(data)))
    if r != 0 {
        return fmt.Errorf("failure occurred in handling data.")
    }
    return nil
}

func (self *Detector) End() {
    C.uchardet_data_end(self.ud)
}

func (self *Detector) Reset() {
    C.uchardet_reset(self.ud)
}

func (self *Detector) Charset() (string, error) {
    r := C.uchardet_get_charset(self.ud)
    rr := C.GoString(r)
    if rr == "" {
        return "", fmt.Errorf("failed in detection.")
    }
    return rr, nil
}

func NewDetector() *Detector {
    r := C.uchardet_new()
    return &Detector{r}
}
